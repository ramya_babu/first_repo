package com.example.springsecurityProfileService1.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.springsecurity.OAuthserver.commons.model.Customer;


@Repository
public interface CustomerRepository  extends JpaRepository<Customer,Integer> {
}