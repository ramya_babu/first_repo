package com.example.springsecurityProfileService1.service;
import java.util.List;

import com.example.springsecurity.OAuthserver.commons.model.Customer;





public interface CustomerService {


    Customer save(Customer customer);

    Customer fetchById(int profileId);

    List<Customer> fetchAllProfiles();
}